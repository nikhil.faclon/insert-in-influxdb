const { influx, optimizedFetchQuery } = require("../initialize/influx");
const _ = require("lodash");

function getDatatype(device) {
  return new Promise((resolve, reject) => {
    influx
      .query(`show field keys from ${device}`)
      .then((response) => {
        if (response) {
          const datatype = _.find(response, { fieldKey: "value" });
          if (datatype) resolve(datatype);
          else resolve({ fieldKey: "value", fieldType: "null" });
        } else {
          resolve({ fieldKey: "value", fieldType: "null" });
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getCountOfSensorByQuery(device) {
  return new Promise((resolve, reject) => {
    const query = influx
      .query(`select count(value) from ${device}`)
      .then((data) => resolve(data))
      .catch(reject);
  });
}

function getData(device, sensor, startTime, endTime, cursor) {
  return new Promise((resolve, reject) => {
    const query = [
      `SELECT value FROM ${device} WHERE "tag" = '${sensor}' AND time >=`,
      "AND time <=",
      "ORDER BY time DESC",
    ];
    optimizedFetchQuery(query, startTime, endTime, true, cursor)
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function getAllData(device, sensor, startTime, endTime, cursor) {
  return new Promise((resolve, reject) => {
    let sensors = sensor.split(",");

    let sensorsQuery = `"tag" = `;
    for (let index = 0; index < sensors.length; index++) {
      sensorsQuery = `${sensorsQuery} '${sensors[index]}'`;
      if (index < sensors.length - 1)
        sensorsQuery = `${sensorsQuery} OR "tag" = `;
    }
    const query = [
      `SELECT * FROM ${device} WHERE ${sensorsQuery} AND time >=`,
      "AND time <=",
      "ORDER BY time",
    ];
    optimizedFetchQuery(query, startTime, endTime, false, cursor)
      .then((data) => resolve(data))
      .catch((error) => reject(error));
  });
}

function getDataByStEt(device, startTime, endTime) {
  return new Promise((resolve, reject) => {
    influx
      .query(
        `select * from ${device} WHERE time>=${startTime} AND time<=${endTime} ORDER BY time DESC`
      )
      .then((data) => resolve(data))
      .catch(reject);
  });
}

module.exports = {
  getDatatype,
  getCountOfSensorByQuery,
  getAllData,
  getData,
  getDataByStEt,
};
