const express = require("express");
const app = express();
const routes = require("./routes");
require("./initialize/influx");

app.use("/api", routes);

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log("server is listening on port 5000");
});
