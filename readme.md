## commit 1 -- "influx"

1).Publisher publishes the data packet of sensor data of a device in a time interval.
2).Subscriber gets the data packet from emqx broker.
3).The received packet is saved into InfluxDB.

## commit 2 -- "added incremental Energy Tag"

1). Added the sensor(tag) Energy to the data packet.
2). The Energy of the last data packet is added to the current data packet Energy(Increments).

## commit 3 -- "insert data of all sensors of a device in report"

1). Retrieve the device data and make a report of Energy Consumed and Time using ExcelJS(EnergyReport1.xlsx).
2). Make report of all sensors of a device(EnergyReport.xlsx).

## commit 4 -- "added readme"

1). added the readme file

## commit 5 -- "implemented 5 functions of API layer Repo"

## commit 6 -- "updated readme"
