const express = require("express");
const router = express.Router();

const {
  getDatatype,
  getCountOfSensorByQuery,
  getAllData,
  getData,
  getDataByStEt,
} = require("../actions");

router.get("/getDatatype", (req, res) => {
  getDatatype(req.query.device)
    .then((data) => res.send(data))
    .catch((error) => res.send({ success: false, errors: [error] }));
});

router.get("/generalCount", (req, res) => {
  getCountOfSensorByQuery(req.query.device)
    .then((data) => res.send(data))
    .catch((error) => res.send({ success: false, errors: [error] }));
});

router.get("/getData", (req, res) => {
  getData(
    req.query.device,
    req.query.sensor,
    req.query.sTime,
    req.query.eTime,
    req.query.cursor
  )
    .then((data) => {
      if (req.query.cursor) res.json(data);
      else res.send(data);
    })
    .catch((error) => res.send({ success: false, errors: [error] }));
});

router.get("/getAllData", (req, res) => {
  getAllData(
    req.query.device,
    req.query.sensor,
    req.query.sTime,
    req.query.eTime,
    req.query.cursor
  )
    .then((data) => {
      if (req.query.cursor) res.json(data);
      else res.send(data);
    })
    .catch((error) => {
      console.log(error);
      res.send({ success: false, errors: [error] });
    });
});

router.get("/getDataByStEt", (req, res) => {
  getDataByStEt(req.query.device, req.query.sTime, req.query.eTime)
    .then((data) => {
      res.send(data);
    })
    .catch((error) => res.send({ success: false, errors: [error] }));
});

module.exports = router;
