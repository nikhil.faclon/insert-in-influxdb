const Influx = require("influx");

const influx = new Influx.InfluxDB({
  host: "localhost",
  port: 8086,
  database: "devices",
});

async function optimizedFetchQuery(query, startTime, endTime, desc, cursor) {
  try {
    let MAX_LIMIT = 10;

    let returnedDataLength = 0;
    let allData = [];

    let queryLimit;
    if (query[2].toLowerCase().includes("limit")) {
      queryLimit = query[2].toLowerCase().split("limit")[1];

      if (queryLimit < MAX_LIMIT) {
        let updatedQuery = `${query[0]} ${startTime} ${query[1]} ${endTime} ${query[2]}`;

        // console.log(updatedQuery);

        const result = await influx.query(updatedQuery);

        if (cursor)
          return {
            data: result,
            cursor: { start: null, end: null, limit: null },
          };
        else return result;
      } else {
        query[2] = query[2].toLowerCase.split("limit")[0];
      }
    }
    let LIMIT = MAX_LIMIT;
    do {
      if (queryLimit) {
        LIMIT =
          allData.length + MAX_LIMIT > queryLimit
            ? queryLimit - allData.length
            : MAX_LIMIT;
      }

      let updatedQuery = `${query[0]} ${startTime} ${query[1]} ${endTime} ${query[2]} LIMIT ${LIMIT}`;

      console.log(updatedQuery);

      let data = await influx.query(updatedQuery);
      returnedDataLength = data.length;

      if (returnedDataLength > 0) {
        if (desc) {
          endTime = data[data.length - 1].time.getNanoTime();

          if (query[1][query[1].length - 1] !== "=") {
            query[1] = `${query[1]}=`;
          }
        } else {
          startTime = data[data.length - 1].time.getNanoTime();

          if (query[0][query[0].length - 1] !== "=") {
            query[0] = `${query[0]}=`;
          }
        }

        if (returnedDataLength === MAX_LIMIT) {
          while (
            desc
              ? endTime === data[data.length - 1].time.getNanoTime()
              : startTime === data[data.length - 1].time.getNanoTime()
          ) {
            data.pop();
          }
        }
      }
      allData = allData.concat(data);
    } while (returnedDataLength >= MAX_LIMIT && !cursor);

    if (cursor) {
      if (returnedDataLength < MAX_LIMIT && desc) endTime = null;
      if (returnedDataLength < MAX_LIMIT && !desc) startTime = null;

      return {
        data: allData,
        cursor: {
          start: startTime,
          end: endTime,
          limit: queryLimit ? queryLimit - allData.length : null,
        },
      };
    }

    return allData;
  } catch (error) {
    console.log(error);
    return error;
  }
}

module.exports = { influx, optimizedFetchQuery };
