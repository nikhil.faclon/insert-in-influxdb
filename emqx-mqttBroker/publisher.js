const mqtt = require("mqtt");
const { options, connectUrl } = require("./config");

const client = mqtt.connect(connectUrl, options);

const devID = "INEM_DEMO";
const topic = `devices/${devID}/data`;
const max = 100;
const min = 10;

client.on("connect", () => {
  let erg = 0;
  setInterval(() => {
    const random = Math.random() * (max - min) + min;
    const random2 = Math.random() * (max - min) + min;
    const random3 = Math.random() * (max - min) + min;

    const rand = Math.floor(Math.random() * 50);
    erg += rand;

    const dataPacket = {
      device: devID,
      time: Date.now(),
      data: [
        {
          tag: "ACTIVE",
          value: random,
        },
        {
          tag: "CUR1",
          value: random2,
        },
        {
          tag: "CUR2",
          value: random2,
        },
        {
          tag: "CUR3",
          value: random3,
        },
        { tag: "Energy", value: erg },
      ],
    };

    client.publish(topic, JSON.stringify(dataPacket));
  }, 60000); //1min
});

console.log("EnergyMeter MQTT client connected:-", client.options.clientId);
