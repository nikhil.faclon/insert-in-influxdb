const mqtt = require("mqtt");
const { influx } = require("../initialize/influx");

const { options, connectUrl } = require("../config");

const client = mqtt.connect(connectUrl, options);

const devID = "INEM_DEMO";
const topic = `devices/${devID}/data`;

influx
  .getDatabaseNames()
  .then((names) => {
    if (!names.includes("devices")) {
      return influx.createDatabase("devices");
    }
  })
  .then(() => {
    console.log("Influx DB connected");
  })
  .catch((error) => console.log({ error }));

client.on("connect", () => {
  client.subscribe(topic, () => {
    console.log(`Subscribe to topic '${topic}'`);
  });
});

client.on("message", async (topic, message) => {
  const deviceData = JSON.parse(message);
  deviceData.data.map(async (d) => {
    await influx.writePoints([
      {
        measurement: deviceData.device,
        tags: { tag: d.tag },
        fields: { value: d.value },
        timestamp: deviceData.time,
      },
    ]);
    console.log("Added data to the Db");
  });
});
