const Excel = require("exceljs");
const lodash = require("lodash");
const { influx } = require("../initialize/influx");

//get data
const getData = async (startTime, endTime, devID) => {
  const data = await influx.query(
    `SELECT * from ${devID} WHERE time >= ${startTime} AND time <= ${endTime} ORDER BY time`
  );
  // console.log(data);
  const energyTime = data.map((d) => {
    return {
      time: new Date(Number(d.time.getNanoTime())).toLocaleString(),
      tag: d.tag,
      value: d.value,
    };
  });

  return energyTime;
};

// getData(1657197242142, 1657200595166, "INEM_DEMO");

//make report
const makeReport = async (startTime, endTime, devId) => {
  const wb = new Excel.Workbook();
  const ws = wb.addWorksheet("Sheet 1");

  const headers = [
    { header: "TIME", key: "time", width: 30 },
    { header: "ACTIVE", key: "active", width: 20 },
    { header: "CUR1", key: "cur1", width: 20 },
    { header: "CUR2", key: "cur2", width: 20 },
    { header: "CUR3", key: "cur3", width: 20 },
    { header: "Energy Consumed", key: "energy", width: 20 },
  ];

  ws.columns = headers;
  let data = await getData(startTime, endTime, devId);

  const groupedByTime = lodash.groupBy(data, (element) => {
    return element.time;
  });

  data = [];
  for (let item in groupedByTime) {
    const row = [];
    row.push(item);
    for (let element of groupedByTime[item]) {
      row.push(element.value);
    }
    data.push(row);
  }

  for (let element of data) {
    ws.addRow(element);
  }

  wb.xlsx
    .writeFile("EnergyReport.xlsx")
    .then(() => {
      console.log("File created");
    })
    .catch((err) => {
      console.log(err.message);
    });
};

makeReport(1657530322724, 1657535896660, "INEM_DEMO");
